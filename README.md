# BBP Job Board System

#### SEO module compatibility

The board is designed to work with the Innoweb SEO module, the Cyber Duck SEO module and the Wilr sitemaps module.
For the most part, integration with these services will be seamless.
If using the Wilr module, a line needs to be added to the site config to register the data.  Please see the changelog for version 2.3.0 for details.



#### Elastic configuration

The job board Elastic storage fields are configured in yml.  Each field can take a number of configuration options.  See the source code for a full list.

```
"fieldname":
  "type": "text"
  "searchable": true
  "filterable": false
  "label": "Some field"
  "cmssummaryfield": true
  "urlfield": true
  "titlefield": true
  "hidden": false
  "precision": 0
  "frontend-field": "checkbox"
  "multifilter": false
  "objectclass": "FQCN\Class"
  "sortfield": true
```

#### Front end info
- Vue https://vuejs.org/
- Vue building https://cli.vuejs.org/
- Storage https://pinia.vuejs.org/
- Persistent storage https://github.com/prazdevs/pinia-plugin-persistedstate
