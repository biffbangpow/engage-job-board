<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\Forms\NumericField;

class NumberField implements JobField
{
    use FieldCommon;

    /**
     * @inheritDoc
     */
    public function getDBFieldType(): string
    {
        $precision = (isset($this->fieldData['precision'])) ? $this->fieldData['precision'] : 0;
        switch ($precision) {
            case ($precision > 0):
                return 'float';
            default:
                return 'integer';
        }
    }


    /**
     * @inheritDoc
     */
    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        $precision = (isset($this->fieldData['precision'])) ? $this->fieldData['precision'] : 0;
        $field = NumericField::create($fieldName, $fieldLabel);
        $field->setScale($precision);
        return $field;
    }


    /**
     * @return int
     */
    public function getDefaultValue()
    {
        return 0;
    }
}
