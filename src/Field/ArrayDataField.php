<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBHTMLText;

class ArrayDataField implements JobField
{
    use FieldCommon;

    /**
     * Get the data as an escaped HTML field for templates
     * @param $data
     * @return DBHTMLText|mixed
     */
    public function getDataForOutput($data)
    {
        $items = unserialize($data);
        if (count($items) > 0) {
            $list = implode('</li><li>', $items);
            return DBField::create_field('HTMLText', '<ul><li>' . $list . '</li></ul>');
        }
    }


    /**
     * Serialize the data for storage
     * @param $data
     * @return mixed|string
     */
    public function formatForStorage($data)
    {
        if (!is_array($data)) {
            $data = (array)$data;
        }
        return serialize($data);
    }


    /**
     * Return a false CMS field, since it doesn't make much sense to mess around
     * trying to represent an array in the CMS.. just use an HTML field!
     * @return false
     */
    public function CMSField()
    {
        return false;
    }


    public function getAnalyser(): ?string
    {
        return 'stemmed_analyzer';
    }
}
