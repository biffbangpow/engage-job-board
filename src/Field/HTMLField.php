<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\ORM\FieldType\DBHTMLText;

class HTMLField implements JobField
{

    use FieldCommon;

    /**
     * Get the data as an escaped HTML field for templates
     * @param $data
     * @return DBHTMLText|mixed
     */
    public function getDataForOutput($data)
    {
        return DBField::create_field('HTMLText', $data);
    }

    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        return HTMLEditorField::create($fieldName, $fieldLabel);
    }

    public function getAnalyser(): ?string
    {
        return 'stemmed_analyzer';
    }

}
