<?php

namespace BiffBangPow\JobBoard\Field;

class HiddenField implements JobField
{
    use FieldCommon;

    public function getDBFieldType(): string
    {
        return "keyword";
    }

    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        return \SilverStripe\Forms\HiddenField::create($fieldName, $fieldLabel);
    }

}
