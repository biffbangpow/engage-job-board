<?php

namespace BiffBangPow\JobBoard\Field;

class DummyField implements JobField
{
    use FieldCommon;

    /**
     * Return an empty array, since we don't want this field type to actually store any data
     * @param string $fieldName
     * @return array
     */
    public function getFieldConfig(string $fieldName): array
    {
        return [];
    }

    public function getCMSField()
    {
        return false;
    }
}