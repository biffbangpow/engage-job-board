<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Extensible;

class DateField implements JobField
{
    use FieldCommon;
    use Extensible;
    use Configurable;

    private static $output_date_format = 'd/m/Y';

    public function getDBFieldType(): string
    {
        return "date";
    }

    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        return \SilverStripe\Forms\DateField::create($fieldName, $fieldLabel);
    }

    public function formatForStorage($data)
    {
        return $data;
    }

    public function getDefaultValue()
    {
        return '';
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function getDataForOutput($data)
    {
        $format = $this->config()->get('output_date_format');
        $timestamp = strtotime($data);
        $data = date($format, $timestamp);
        $this->extend('updateDataForOutput', $data);
        return $data;
    }
}
