<?php

namespace BiffBangPow\JobBoard\Field;

use BiffBangPow\JobBoard\Forms\DynamicDropdownField;
use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Page\JobsPage;
use SilverStripe\Control\Controller;
use SilverStripe\TagField\StringTagField;
use SilverStripe\TagField\TagField;

class KeywordField implements JobField
{
    use FieldCommon;

    public function getDBFieldType(): string
    {
        return "keyword";
    }

    /**
     * Prepare the data for storage.  If it's been tagified then we need to decode it
     * Currently, only a single value is stored due to the limitations of the way the search syntax is
     * constructed.   See BJB-18 for more.
     * @param $data
     * @return mixed|string
     */
    public function formatForStorage($data)
    {
        if ((isset($this->fieldData['tagify'])) && ($this->fieldData['tagify'] === true)) {
            $tags = (is_array($data)) ? $data : explode(",", (string)$data);
            return trim(array_shift($tags));
        }

        return ($data) ? trim($data) : $data;
    }


    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;

        if ((isset($this->fieldData['tagify'])) && ($this->fieldData['tagify'] === true)) {
            $field = StringTagField::create(
                $fieldName,
                $fieldLabel,
                ElasticHelper::getDistinctValuesForField($fieldName),
                []
            )
                ->setCanCreate(true)
                ->setShouldLazyLoad(false)
                ->setDescription('Please note:  only the first value is stored');
        } else {
            $field = \SilverStripe\Forms\TextField::create($fieldName, $fieldLabel);
        }

        return $field;
    }

    /**
     * Our tag data is stored as a comma-separated string.  Even with a single value, we still need
     * an array for the tagfield
     * @param $data
     * @return false|mixed|string[]
     */
    public function formatForCMSField($data)
    {
        if ((isset($this->fieldData['tagify'])) && ($this->fieldData['tagify'] === true)) {
            return explode(",", $data);
        }
        return $data;
    }

}
