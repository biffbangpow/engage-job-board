<?php

namespace BiffBangPow\JobBoard\Field;

/**
 * @property array fieldData
 */
trait FieldCommon
{
    /**
     * The definition of the application field
     * @var array
     */
    private array $fieldData = [];


    /**
     * Set the config data for the field in a class property
     * @param array $fieldDef
     * @return void
     */
    public function setFieldData(array $fieldDef)
    {
        $this->fieldData = $fieldDef;
    }

    /**
     * Get the Elastic config array for this field type
     * @param string $fieldName
     * @return array[]
     */
    public function getFieldConfig(string $fieldName): array
    {
        $fieldConfig = [];
        $fieldConfig['type'] = $this->getDBFieldType();
        if ($this->getAnalyser() != '') {
            $fieldConfig['analyzer'] = $this->getAnalyser();
        }
        if ($this->getExtraOptions() !== false) {
            $fieldConfig = array_merge($fieldConfig, $this->getExtraOptions());
        }
        return [$fieldName => $fieldConfig];
    }

    /**
     * Get the Elastic field type for this field
     * Defaults to text for safety
     * @return string
     */
    public function getDBFieldType(): string
    {
        return "text";
    }

    /**
     * Get the data analyzer for this field
     * so we can add it to the config
     * @return string|null
     */
    public function getAnalyser(): ?string
    {
        return null;
    }

    /**
     * Get any additional options for this field
     * to be added to the Elastic config
     * @return false|array
     */
    public function getExtraOptions()
    {
        return false;
    }

    /**
     * Return the data, transformed any way we need it for the application
     * @param $data
     * @return mixed
     */
    public function getDataForOutput($data)
    {
        return $data;
    }

    /**
     * Transform the data into a storable format
     * @param $data
     * @return mixed
     */
    public function formatForStorage($data)
    {
        return ($data) ? trim($data) : $data;
    }

    /**
     * Return a CMS field for this class, assuming it's not hidden by the config
     * @return false|mixed
     */
    public function getCMSField()
    {
        if ((isset($this->fieldData['hidden'])) && ($this->fieldData['hidden'] === true)) {
            return false;
        } else {
            return $this->CMSField();
        }
    }

    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        return \SilverStripe\Forms\TextField::create($fieldName, $fieldLabel);
    }


    /**
     * Return the data for use in the CMS, by default, without modification
     * @param $data
     * @return mixed
     */
    public function formatForCMSField($data)
    {
        return $data;
    }

    /**
     * Get a safe, default value for the field type
     * @return string
     */
    public function getDefaultValue()
    {
        return '';
    }
}
