<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\DataObject;

class DataObjectLinkField implements JobField
{
    use FieldCommon;


    /**
     * @inheritDoc
     */
    public function getDBFieldType(): string
    {
        return "integer";
    }


    /**
     * Get the dataobject associated with this record
     * @param $data
     * @return DataObject|null
     */
    public function getDataForOutput($data): ?DataObject
    {
        if ($data > 0) {
            $doClass = $this->fieldData['objectclass'];
            return DataObject::get_by_id($doClass, $data);
        }
        return null;
    }


    public function CMSField()
    {
        //Special cases are needed for image / file, since they have specific controls
        //All others just use a dropdown field

        if (!isset($this->fieldData['objectclass'])) {
            return false;
        }

        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;

        if (($this->fieldData['objectclass'] === Image::class) || ($this->fieldData['objectclass'] === File::class)) {
            return UploadField::create($fieldName, $fieldLabel);
        }
        else {
            $source = DataObject::get($this->fieldData['objectclass'])->map('ID', $this->fieldData['objecttitlefield']);
            return DropdownField::create($fieldName, $fieldLabel, $source)->setEmptyString('Please select:');
        }
    }

    /**
     * @return int
     */
    public function getDefaultValue()
    {
        return 0;
    }
}
