<?php

namespace BiffBangPow\JobBoard\Field;

class TextField implements JobField
{
    use FieldCommon;

    public function getAnalyser(): ?string
    {
        return 'stemmed_analyzer';
    }
}
