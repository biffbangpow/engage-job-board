<?php

namespace BiffBangPow\JobBoard\Field;

use SilverStripe\Forms\CheckboxField;

class BooleanField implements JobField
{
    use FieldCommon;

    /**
     * @return string
     */
    public function getDBFieldType(): string
    {
        return "boolean";
    }

    public function CMSField()
    {
        $fieldName = (isset($this->fieldData['fieldname'])) ? $this->fieldData['fieldname'] : 'Error-Nolabel';
        $fieldLabel = (isset($this->fieldData['label'])) ? $this->fieldData['label'] : $fieldName;
        return CheckboxField::create($fieldName, $fieldLabel);
    }

    public function formatForStorage($data)
    {
        return (($data === true) || ($data === 1));
    }

    /**
     * @return false
     */
    public function getDefaultValue()
    {
        return 0;
    }

}
