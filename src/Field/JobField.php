<?php

namespace BiffBangPow\JobBoard\Field;


interface JobField
{

    /**
     * @param array $fieldDef
     * @return void
     */
    public function setFieldData(array $fieldDef);

    /**
     * Get the elastic field type for this field
     * @return string
     */
    public function getDBFieldType(): string;

    /**
     * Get the elastic analyser for this field
     * @return string|null
     */
    public function getAnalyser(): ?string;

    /**
     * Get additional options for this field
     * @return array|false
     */
    public function getExtraOptions();

    /**
     * Return the data with any formatting which may be required
     * @return mixed
     */
    public function getDataForOutput($data);

    /**
     * Get the config required to build this field in the index
     * @param string $fieldName
     * @return array
     */
    public function getFieldConfig(string $fieldName): array;

    /**
     * Formats the data
     * @param $data
     * @return mixed
     */
    public function formatForStorage($data);

    /**
     * Get the Silverstripe field for use in the CMS assuming it's not hidden
     * @return mixed
     */
    public function getCMSField();

    /**
     * @return mixed
     */
    public function CMSField();

    /**
     * If required, transform the stored data for the CMS field
     * @param $data
     * @return mixed
     */
    public function formatForCMSField($data);

    /**
     * Get a default value for the field type
     * @return mixed
     */
    public function getDefaultValue();
}
