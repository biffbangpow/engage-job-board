<?php

namespace BiffBangPow\JobBoard\Page;

use BiffBangPow\JobBoard\Control\JobsPageController;
use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Helper\FieldHelper;
use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

class JobsPage extends \Page
{
    private static $table_name = 'BBP_JobsPage';
    private static $controller_name = JobsPageController::class;

    /**
     * Get the markup for all our filters
     * @return ArrayList
     */
    public function getFilters()
    {
        $filterList = ArrayList::create();

        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['filterable'])) && ($config['filterable'] === true)) {
                $filterList->push(ArrayData::create([
                    'Filter' => FieldHelper::getFilterMarkup($fieldName)
                ]));
            }
        }

        return $filterList;
    }

    /**
     * See if any of the defined fields are flagged as searchable
     * @return bool
     */
    public function hasSearch(): bool
    {
        $searchFields = ElasticHelper::getSearchFields();
        return (count($searchFields) > 0);
    }
}
