<?php

namespace BiffBangPow\JobBoard\Dev;

use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Dev\BuildTask;

class UpdateEngineTask extends BuildTask
{
    private static $segment = 'UpdateElastic';
    protected $title = 'Update the Elastic engine fields';
    protected $description = 'Update the Elastic engine fields based on YML config files.  Run when settings have been changed';

    public function run($request)
    {
        $indexName = JobListing::getElasticIndexName();
        echo "<h1>Updating " . $indexName . "</h1>";
        ElasticHelper::jobsIndex(false);
    }
}
