<?php

namespace BiffBangPow\JobBoard\Dev;

use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\Queries\SQLDelete;
use SilverStripe\ORM\Queries\SQLInsert;

class SyncJobBoardTask extends BuildTask
{
    private static $segment = 'SyncJobBoard';
    protected $title = 'Sync the local installation to the remote jobs database';
    protected $description = 'WARNING: this is a destructive task which will remove all the local job records';

    public function run($request)
    {
        $indexName = JobListing::getElasticIndexName();
        echo "<p>Emptying local database...";
        $jobListings = JobListing::get();
        foreach ($jobListings as $jobListing) {
            $del = SQLDelete::create();
            $del->setFrom('BBP_JobListings')
                ->setWhere(['ID' => $jobListing->ID]);
            $del->execute();
        }
        echo " done</p>";

        $esListings = ElasticHelper::getJobsByFilterParams([], '/null', false, 0, 1000);

        if ((is_array($esListings)) && (isset($esListings['Jobs']))) {
            foreach ($esListings['Jobs'] as $jobRecord) {
                $linkedID = $jobRecord['Data']['linkedid'];
                echo "<br>" . $jobRecord['ID'] . " - " . $jobRecord['Data']['jobtitle'] . " (Linked ID " . $linkedID . ")";
                $check = JobListing::get()->byID($linkedID);
                if ($check) {
                    echo '<span style="color: red;"> - Duplicate local record ID - Skipping!</span>';
                } else {
                    $insert = SQLInsert::create();
                    $insert->setInto('BBP_JobListings')
                        ->addAssignments([
                            'ID' => $linkedID,
                            'RecordID' => $jobRecord['ID']
                        ]);
                    $insert->execute();
                    echo " - Record added locally";
                }
            }
            echo "<p>All done</p>";
        } else {
            echo "<p>Nothing to sync.  Finishing</p>";
        }

    }
}
