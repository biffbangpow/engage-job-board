<?php

namespace BiffBangPow\JobBoard\Dev;

use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Dev\BuildTask;

class ConfigureEngineTask extends BuildTask
{
    private static $segment = 'ConfigureElastic';
    protected $title = 'Configure the Elastic engine index';
    protected $description = 'Build the Elastic engine index based on YML config files.  Run only once!';

    public function run($request)
    {
        $indexName = JobListing::getElasticIndexName();
        echo "<h1>Building " . $indexName . "</h1>";

        ElasticHelper::jobsIndex(true);
    }
}
