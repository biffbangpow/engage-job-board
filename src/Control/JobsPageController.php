<?php

namespace BiffBangPow\JobBoard\Control;


use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Helper\FieldHelper;
use BiffBangPow\JobBoard\Model\JobApplication;
use BiffBangPow\JobBoard\Model\JobListing;
use BiffBangPow\JobBoard\Page\JobsPage;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use ReflectionException;
use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Flushable;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Core\Manifest\ModuleLoader;
use SilverStripe\Forms\Form;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\Requirements;

/**
 * @method JobsPage data()
 */
class JobsPageController extends \PageController implements Flushable
{
    const CONTACT_CAPTCHA_ACTION = 'applicationform';
    const FIELD_SETTINGS_CACHE_KEY = 'fe-field-settings';
    const FIELD_SETTINGS_CACHE_LIFE = 43200;
    const JOB_VIEW_SEGMENT = 'view';

    private static $allowed_actions = [
        'getresults',
        'getfieldsconfig',
        self::JOB_VIEW_SEGMENT,
        'processApplication'
    ];

    private static $url_handlers = [
        self::JOB_VIEW_SEGMENT . '/$ID!/$Slug' => 'view',
        'fieldData/$FieldName' => 'fieldData'
    ];

    private $canonicalURL;
    private $jobSummary;
    private $jobTitle;
    private $jobSEOTitle;

    /**
     * General function to build a page title
     * @return string|void
     */
    public function buildSEOMetaTitle()
    {
        $seoSuffix = SiteConfig::current_site_config()->JobPageSEOSuffix ?? null;

        if ($this->jobSEOTitle != '') {
            return $this->jobSEOTitle . (($seoSuffix) ?? '');
        }
        if ($this->jobTitle) {
            return sprintf(
                "%s%s",
                $this->jobTitle,
                $seoSuffix
            );
        }
    }

    /**
     * Function to return the meta title for the innoweb seo module
     * @return string|null
     */
    public function getSocialMetaTitle()
    {
        return $this->buildSEOMetaTitle();
    }

    /**
     * Function to return the metadescription for the innoweb seo module
     * @return mixed
     */
    public function getSocialMetaDescription()
    {
        return $this->jobSummary;
    }

    /**
     * Function to feed the canonical link into the innoweb SEO module
     * @return mixed
     */
    public function getSocialMetaCanonicalURL()
    {
        return $this->canonicalURL;
    }

    /**
     * Function to get the canonical link for the Cyber duck seo module
     * @param $query
     * @return string
     */
    public function getPageCanonical($query = null)
    {
        return $this->canonicalURL;
    }

    /**
     * Gets the page url for the og data in the cyber duck module
     * @return string
     */
    public function getPageURL() {
        return $this->canonicalURL;
    }

    /**
     * Function to return the page title for the Cyberduck SEO module
     * @return string|null
     */
    public function getPageMetaTitle()
    {
        return $this->buildSEOMetaTitle();
    }


    /**
     * FUnction to return the job summary for the meta in the Cyberduck SEO module
     * @return string
     */
    public function getPageMetaDescription()
    {
        return $this->jobSummary;
    }

    public function index()
    {
        Requirements::css('biffbangpow/engage-job-board:client/dist/css/app.css');
        Requirements::javascript('biffbangpow/engage-job-board:client/dist/js/app.js');
        Requirements::javascript('biffbangpow/engage-job-board:client/dist/js/chunk-vendors.js');
        $this->canonicalURL = $this->data()->AbsoluteLink();

        //We need to add some overrides here if we're using the Cyberduck module
        $module = ModuleLoader::getModule('cyber-duck/silverstripe-seo');
        if ($module) {
            $this->jobTitle = $this->MetaTitle ?? $this->Title;
            $this->jobSummary = $this->MetaDescription;
        }
        return $this->render();
    }

    public function view(HTTPRequest $request)
    {
        $jobListing = ElasticHelper::getRecordByLinkedID((int)$request->param('ID'));

        if ($jobListing === false) {
            return $this->httpError(404);
        }

        Injector::inst()->get(LoggerInterface::class)->info(print_r($jobListing, true));

        $canonicalLink = Controller::join_links([
            $this->data()->AbsoluteLink(),
            self::JOB_VIEW_SEGMENT,
            $jobListing->urlsegment
        ]);

        $jobMeta = [
            'jobtitle' => $jobListing->jobtitle,
            'seotitle' => $jobListing->seotitle,
            'canonical' => $canonicalLink,
            'summary' => $jobListing->summary
        ];

        $this->extend("updateMetaDataContent", $jobMeta, $jobListing);

        $this->jobSEOTitle = $jobMeta['seotitle'];
        $this->jobTitle = $jobMeta['jobtitle'];
        $this->canonicalURL = $jobMeta['canonical'];
        $this->jobSummary = $jobMeta['summary'];

        $siteKey = Environment::getEnv('CAPTCHA_SITE_KEY');
        $secretKey = Environment::getEnv('CAPTCHA_SECRET_KEY');

        Requirements::css('biffbangpow/engage-job-board:client/dist/application/css/modal.css');
        Requirements::javascript('biffbangpow/engage-job-board:client/dist/application/javascript/micromodal.min.js');
        Requirements::javascript('biffbangpow/engage-job-board:client/dist/application/javascript/pristine.min.js');
        Requirements::javascript('biffbangpow/engage-job-board:client/dist/application/javascript/jobapplication.js');

        if (($secretKey != "") && ($siteKey != "")) {
            Requirements::javascript('https://www.google.com/recaptcha/api.js', [
                'type' => false,
                'preload' => true,
                'async' => true,
            ]);
        }

        $this->Layout = $jobListing->renderWith('BiffBangPow/JobBoard/Page/Layout/JobsPage_view', [
            'ApplicationForm' => $this->ApplicationForm($jobListing->_id),
        ]);

        return $this->render();
    }


    /**
     * Returns the filter field configuration string
     * @return false|string
     */
    public function getfieldsconfig()
    {
        $cache = Injector::inst()->get(CacheInterface::class . '.BBPJBFieldSettings');
        if ($cache->has(self::FIELD_SETTINGS_CACHE_KEY)) {
            return $cache->get(self::FIELD_SETTINGS_CACHE_KEY);
        }

        $fields = [];

        //Start with the search box
        if ($this->data()->hasSearch()) {
            $searchFieldName = JobListing::config()->get('search_field_name');
            $fields['fields'][$searchFieldName] = [
                'type' => 'textinput',
                'values' => 'null',
                'label' => 'Search',
                'placeholder' => JobListing::config()->get('search_field_placeholder'),
            ];
        }

        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if (
                (isset($config['filterable']))
                && ($config['filterable'] === true)
                && (isset($config['frontend-field']))
                && ($config['frontend-field'] != '')
            ) {
                $feDefs = FieldHelper::getFrontEndDefs($fieldName, $config);
                if ((is_array($feDefs)) && (count($feDefs) > 0)) {
                    $fields['fields'][$fieldName] = $feDefs;
                }
            }
        }

        $fields['sort']['fieldname'] = JobListing::config()->get('sort_field_name');
        $fields['sort']['placeholder'] = JobListing::config()->get('sort_field_placeholder');
        $fields['sort']['defaultsort'] = JobListing::config()->get('results_default_sort');

        $sortFields = ElasticHelper::getSortFields();
        foreach ($sortFields as $fieldName => $config) {
            $fields['sort']['fields'][$fieldName] = $config['label'];
        }

        $res = json_encode($fields);
        $cache->set(self::FIELD_SETTINGS_CACHE_KEY, $res, self::FIELD_SETTINGS_CACHE_LIFE);

        return $res;
    }


    /**
     * @param HTTPRequest $request
     * @return string
     * @throws ReflectionException
     */
    public function getresults(HTTPRequest $request)
    {
        $from = ((int)$request->postVar('from') > 0) ? (int)$request->postVar('from') : 0;
        $size = ((int)$request->postVar('size') > 0) ? (int)$request->postVar('size') : 24;
        $baseURL = rtrim($this->data()->Link(), '/').'/';

        return json_encode(ElasticHelper::getJobListings($request, $baseURL, false, $from, $size), true);
    }


    /**
     * @param string|null $jobID
     * @return Form
     * @throws ReflectionException
     */
    public function ApplicationForm(string $jobID = null): Form
    {
        $formClass = JobApplication::config()->get('form_class');
        $ref = new \ReflectionClass($formClass);

        /**
         * @var Form $form
         */
        $form = $ref->newInstanceArgs([
            $this,
            'ApplicationForm',
            $jobID,
        ]);

        $form->setFormAction($this->Link('processApplication'));
        return $form;
    }

    public function processApplication(HTTPRequest $request)
    {
        $formClass = JobApplication::config()->get('form_class');
        $ref = new \ReflectionClass($formClass);

        /**
         * @var Form $form
         */
        $form = $ref->newInstanceArgs([
            $this,
            'ApplicationForm',
            0,
        ]);

        return $form->handle($request);
    }

    /**
     * Clear the field settings cache
     * Run during any flush operation, and after a job is saved
     * @return void
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public static function flush()
    {
        Injector::inst()->get(CacheInterface::class . '.BBPJBFieldSettings')->clear();
    }

    /**
     * Get the URL segment we use for the job view page
     * @return mixed
     */
    public static function getViewSegment()
    {
        return self::JOB_VIEW_SEGMENT;
    }
}

