<?php

namespace BiffBangPow\JobBoard\Extensions;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class JobBoardSiteConfigExtension extends DataExtension
{
    /**
     * @var array
     */
    private static $db = [
        'JobApplicationsToEmail' => 'Varchar(200)',
        'JobApplicationsFromEmail' => 'Varchar(200)',
        'JobPageSEOSuffix' => 'Varchar'
    ];

    /**
     * @param FieldList $fields
     * @return void
     */
    public function updateCMSFields(FieldList $fields)
    {
        $fields->addFieldsToTab('Root.JobBoard', [
            TextField::create('JobApplicationsToEmail')->setDescription('The email address that job applications will be sent to'),
            TextField::create('JobApplicationsFromEmail')->setDescription('The email address that job applications will be sent from'),
            TextField::create('JobPageSEOSuffix', 'Job page title suffix')
                ->setDescription('Added to the end of the page title on a job listing page')
        ]);
    }
}
