<?php

namespace BiffBangPow\JobBoard\Service;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Environment;

class ElasticService
{
    use Configurable;

    private string $elastic_user;
    private string $elastic_password;
    private Client $client;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        $this->elastic_user = Environment::getEnv('ELASTIC_USERNAME');
        $this->elastic_password = Environment::getEnv('ELASTIC_PASSWORD');

        if (($this->elastic_user == '') || ($this->elastic_password == '')) {
            throw new \Exception("Fatal error.  Missing Elastic credentials");
        }

        $this->client = ClientBuilder::create()
            ->setHosts([
                [
                    'host' => Environment::getEnv('ES_HOST'),
                    'port' => Environment::getEnv('ES_PORT'),
                    'scheme' => Environment::getEnv('ES_SCHEME'),
                    'user' => $this->elastic_user,
                    'pass' => $this->elastic_password
                ]
            ])
            //->setSSLVerification(Director::getAbsFile('elasticsearch/config/certs/ca/ca.crt'))
            ->build();


    }

    /**
     * @param $params
     * @return array
     */
    public function createIndex($params)
    {
        return $this->client->indices()->create($params);
    }

    /**
     * @param $params
     * @return array
     */
    public function updateIndexFields($params): array
    {
        return $this->client->indices()->putMapping($params);
    }

    /**
     * @param array $params
     * @return false|mixed
     */
    public function indexDocument(array $params)
    {
        $res = $this->client->index($params);
        return (isset($res['_id'])) ? $res['_id'] : false;
    }

    public function updateDocument(array $params)
    {
        $res = $this->client->update($params);
        return true;
    }

    /**
     * @param array $params
     * @return array
     */
    public function getDocument(array $params): array
    {
        return $this->client->get($params);
    }

    /**
     * @param array $params
     * @return array
     */
    public function deleteDocument(array $params): array
    {
        return $this->client->delete($params);
    }

    /**
     * @param array $params
     * @return array|callable
     */
    public function search(array $params)
    {
        return $this->client->search($params);
    }

    /**
     * @param array $params
     * @return array|callable
     */
    public function deleteByQuery(array $params)
    {
        return $this->client->deleteByQuery($params);
    }

}
