<?php

namespace BiffBangPow\JobBoard\Form\Field;

use SilverStripe\Control\HTTPRequest;

interface FrontendField
{

    /**
     * @param $fieldName
     * @param $fieldConfig
     */
    public function __construct($fieldName, $fieldConfig);

    /**
     * Get the field specification to be sent to the frontend JS
     * @return mixed
     */
    public function getConfigValues();

    /**
     * Get the filter parameters to for the elastic query
     * @param HTTPRequest $request
     * @return mixed
     */
    public function getTermsForFilter(HTTPRequest $request);
}
