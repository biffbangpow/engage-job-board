<?php

namespace BiffBangPow\JobBoard\Form\Field;

use BiffBangPow\JobBoard\Helper\ElasticHelper;
use Psr\Log\LoggerInterface;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Injector\Injector;

class SliderField implements FrontendField
{
    use FieldCommon;
    use Configurable;

    private static $slider_config;

    public function getConfigValues(): array
    {
        $config = $this->getFieldConfig();
        $fieldName = $this->getFieldName();

        if ((isset($config['hide_on_frontend'])) && ($config['hide_on_frontend'] === true)) {
            return [];
        }

        //We must have the related data field to be able to work
        if ((!isset($config['typefieldname'])) || (!isset($config['maxfieldname']))) {
            return '';
        }

        //Get all the possible salary types
        $allTypes = ElasticHelper::getDistinctValuesForField($config['typefieldname']);

        //For each salary type, get the range of salaries
        $values = [];

        $sliderConfig = Config::inst()->get(self::class, 'slider_config');

        foreach ($allTypes as $type) {

            //Get all the values for the low end - based on the field name
            $typeLowValues = ElasticHelper::getDistinctValuesForField($fieldName, false, [
                $config['typefieldname'] => $type
            ]);

            //Get all the values for the high end - based on the related field name
            $typeHighValues = ElasticHelper::getDistinctValuesForField($config['maxfieldname'], false, [
               $config['typefieldname'] => $type
            ]);

            if (count($typeLowValues) > 0) {
                sort($typeLowValues);
                $low = array_shift($typeLowValues);
            } else {
                $low = 0;
            }

            if (count($typeHighValues) > 0) {
                sort($typeHighValues);
                $high = array_pop($typeHighValues);
            } else {
                $high = 100000;
            }

            if (isset($sliderConfig[$type])) {
                $thisTypeConfig = $sliderConfig[$type];

                $values[] = [
                    'unit' => $type,
                    'min' => $low,
                    'max' => $high,
                    'step' => $thisTypeConfig['Step'],
                    'label' => $thisTypeConfig['Label'],
                    'valueFormat' => $thisTypeConfig['ValueFormat']
                ];
            }
        }

        return [
            'multiple' => false,
            'type' => $config['frontend-field'],
            'label' => $config['label'],
            'placeholder' => (isset($config['placeholder'])) ? $config['placeholder'] : '',
            'values' => $values
        ];
    }

    /**
     * @param HTTPRequest $request
     * @return array|false
     */
    public function getTermsForFilter(HTTPRequest $request)
    {
        $config = $this->getFieldConfig();
        $typeFieldName = $config['typefieldname'];
        $maxFieldName = $config['maxfieldname'];

        $fieldBaseName = $this->getFieldName();
        $unit = $request->postVar($fieldBaseName . '_unit');
        $min = (int)$request->postVar($fieldBaseName . '_min');
        $max = (int)$request->postVar($fieldBaseName . '_max');

        if (($unit == '') || ($max < $min) || ($typeFieldName == '')) {
            return false;
        }

        $term = [
            'term' => [
                $typeFieldName => [
                    'value' => $unit
                ]
            ]
        ];

        $lowrange = [
            'range' => [
                $fieldBaseName => [
                    'lte' => $max
                ]
            ]
        ];

        $highrange = [
            'range' => [
                $maxFieldName => [
                    'gte' => $min
                ]
            ]
        ];

        return [
            $term,
            $lowrange,
            $highrange
        ];
    }
}



