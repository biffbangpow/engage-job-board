<?php

namespace BiffBangPow\JobBoard\Form\Field;

use BiffBangPow\JobBoard\Helper\ElasticHelper;
use SilverStripe\Control\HTTPRequest;

trait FieldCommon
{
    protected $fieldName;
    protected $fieldConfig;

    public function __construct($fieldName, $fieldConfig)
    {
        $this->fieldName = $fieldName;
        $this->fieldConfig = $fieldConfig;
    }

    /**
     * @return array
     */
    public function getConfigValues(): array
    {
        $config = $this->getFieldConfig();
        $fieldName = $this->getFieldName();

        if ((isset($config['hide_on_frontend'])) && ($config['hide_on_frontend'] === true)) {
            return [];
        }

        return [
            'type'               => $config['frontend-field'],
            'label'              => $config['label'],
            'multiple'           => (isset($config['multifilter']) && ($config['multifilter'] === true)),
            'values'             => ElasticHelper::getDistinctValuesForField($fieldName, false),
            'placeholder'        => (isset($config['placeholder'])) ? $config['placeholder'] : '',
            'labeltexttransform' => (isset($config['labeltexttransform'])) ? $config['labeltexttransform'] : '',
        ];
    }

    /**
     * Build the filter terms for a simple boolean match field
     * @param HTTPRequest $request
     * @return array[]|\array[][]
     */
    public function getTermsForFilter(HTTPRequest $request)
    {

        $fieldName = $this->getFieldName();
        $matchValue = $request->postVar($fieldName);
        if ($matchValue == "") {
            return false;
        }

        if (is_array($matchValue)) {
            $res = ['terms' => [$fieldName => $matchValue]];
        } else {
            $res = ['term' => [
                $fieldName => ['value' => $matchValue],
            ]];
        }
        return [$res];
    }

    protected function getFieldConfig()
    {
        return $this->fieldConfig;
    }

    protected function getFieldName()
    {
        return $this->fieldName;
    }
}
