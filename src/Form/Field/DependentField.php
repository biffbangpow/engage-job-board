<?php

namespace BiffBangPow\JobBoard\Form\Field;

use BiffBangPow\JobBoard\Helper\ElasticHelper;

class DependentField implements FrontendField
{
    use FieldCommon;

    public function getConfigValues(): array
    {
        $config = $this->getFieldConfig();
        $fieldName = $this->getFieldName();

        if ((isset($config['hide_on_frontend'])) && ($config['hide_on_frontend'] === true)) {
            return [];
        }

        return [
            'type'        => $config['frontend-field'],
            'label'       => (isset($config['label'])) ? $config['label'] : '',
            'multiple'    => false,
            'values'      => ElasticHelper::getDependentFieldValues($fieldName, $config),
            'placeholder' => (isset($config['placeholder'])) ? $config['placeholder'] : '',
        ];
    }
}
