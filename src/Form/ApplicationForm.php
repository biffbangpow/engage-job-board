<?php

namespace BiffBangPow\JobBoard\Form;

use BiffBangPow\JobBoard\Helper\CaptchaHelper;
use BiffBangPow\JobBoard\Control\JobsPageController;
use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Model\JobApplication;
use BiffBangPow\JobBoard\Page\JobsPage;
use Psr\Log\LoggerInterface;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Folder;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\RequestHandler;
use SilverStripe\Core\Environment;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\FileField;
use SilverStripe\Forms\Form;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\HiddenField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\ValidationException;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\ArrayData;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mime\Exception\RfcComplianceException;

class ApplicationForm extends Form
{


    /**
     * @param RequestHandler|null $controller
     * @param string $name
     * @param $jobID
     */
    public function __construct(RequestHandler $controller = null, string $name = self::DEFAULT_NAME, $jobID = null)
    {
        $fields = FieldList::create(
            TextField::create('Name'),
            TextField::create('Phone'),
            EmailField::create('Email')
                ->setAttribute('data-pristine-type', 'email'),
            TextareaField::create('Message'),
            FileField::create('CVFile')
                ->setFolderName('CVUploads')
                ->setAllowedExtensions(['pdf', 'doc', 'docx', 'rtf']),
            CheckboxField::create('Terms'),
            HiddenField::create('JobID')->setValue($jobID)
        );

        $this->extend('updateApplicationFormFields', $fields);

        $formAction = FormAction::create('processApplication')->setTitle(_t(__CLASS__ . '.applynow', 'Apply Now'));

        //We need to ensure we're using a complete form action template, to be sure the right attributes
        //are added.  These are often overridden, especially in older projects!
        $formAction->setTemplate('BiffBangPow/JobBoard/Forms/FormAction');

        $siteKey = Environment::getEnv('CAPTCHA_SITE_KEY');
        $secretKey = Environment::getEnv('CAPTCHA_SECRET_KEY');

        if (($siteKey != "") && ($secretKey != "")) {
            $formAction->setAttribute('data-sitekey', CaptchaHelper::getRecaptchaKey())
                ->addExtraClass('g-recaptcha cta')
                ->setAttribute('data-callback', 'handleApplication')
                ->setAttribute('data-action', JobsPageController::CONTACT_CAPTCHA_ACTION);
        }

        $actions = FieldList::create(
            $formAction,
            LiteralField::create('message', '<div id="applicationform-message"></div>')
        );
        $required = RequiredFields::create(
            'Name',
            'Email',
            'Message',
            'CVFile',
            'Terms'
        );

        parent::__construct($controller, $name, $fields, $actions, $required);
    }

    /**
     * @param HTTPRequest $request
     * @return HTTPResponse
     * @throws ValidationException
     */
    public function handle(HTTPRequest $request)
    {
        $siteKey = Environment::getEnv('CAPTCHA_SITE_KEY');
        $secretKey = Environment::getEnv('CAPTCHA_SECRET_KEY');

        if (($siteKey != "") && ($secretKey != "")) {
            $validation = CaptchaHelper::validateRequestSecurity($request, JobsPageController::CONTACT_CAPTCHA_ACTION);
            if ($validation !== true) {
                return $validation;
            }
        }

        $data = $request->postVars();

        $config = SiteConfig::current_site_config();
        $toMail = $config->JobApplicationsToEmail;
        $fromMail = $config->JobApplicationsFromEmail;

        $jobsPage = JobsPage::get()->limit(1)->first();
        $baseURL = $jobsPage->AbsoluteLink();
        $jobData = ElasticHelper::getRecordByID($data['JobID']);
        $jobLink = sprintf('%sview/%s', $baseURL, $jobData['_source']['urlsegment']);
        $jobInfo = $jobData['_source']['jobtitle'];
        $jobInfo .= ($jobData['_source']['jobref'] != '') ? ' (' . $jobData['_source']['jobref'] . ')' : '';

        $jobApplication = new JobApplication($data);

        $mail = Email::create()
            ->setSubject('Job Application Email')
            ->setTo($toMail)
            ->setFrom($fromMail)
            ->setReplyTo($data['Email'])
            ->setHTMLTemplate('Email/JobApplication')
            ->setData([
                'FormData' => ArrayData::create($data),
                'JobLink' => $jobLink,
                'JobName' => $jobInfo
            ]);

        if ($data['CVFile']['size'] > 0) {
            $file = new File();
            $file->setFromLocalFile($data['CVFile']['tmp_name'], $data['CVFile']['name']);
            $folder = Folder::find_or_make('JobBoardCVs');
            $file->ParentID = $folder->ID;
            $file->write();
            $file->updateFilesystem();
            $mail->addAttachment(Director::baseFolder() . '/public' . $file->getSourceURL());
            $jobApplication->CVFileID = $file->ID;
        }

        $jobApplication->write();
        $this->extend('updateJobApplication', $jobApplication);

        try {
            $mail->send();
            return json_encode([
                'status' => 1,
                'message' => 'Thankyou for your submission'
            ]);
        } catch (\Exception|TransportException|RfcComplianceException $e) {
            Injector::inst()->get(LoggerInterface::class)->error($e->getMessage());
            return json_encode([
                'status' => -1,
                'message' => 'Sorry, there was a problem with your submission.  Please contact us for assistance'
            ]);
        }
    }
}
