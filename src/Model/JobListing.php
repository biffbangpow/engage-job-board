<?php

namespace BiffBangPow\JobBoard\Model;

use BiffBangPow\JobBoard\Control\JobsPageController;
use BiffBangPow\JobBoard\Field\JobField;
use BiffBangPow\JobBoard\Helper\ElasticHelper;
use BiffBangPow\JobBoard\Page\JobsPage;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Environment;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\View\Parsers\URLSegmentFilter;

/**
 * @property DBVarchar RecordID
 */
class JobListing extends DataObject
{
    use Configurable;

    const TITLE_SEPARATOR = ' - ';

    protected static $elasticIndexName;
    protected $triggerBeforeWriteActions = true;
    protected $triggerAfterWriteActions = true;

    private static $fields = [];
    private static $field_mappings = [];
    private static $frontend_field_mappings = [];
    private static $search_field_name = 'keywordsearch';
    private static $search_field_placeholder = 'Search';
    private static $sort_field_name = 'sort';
    private static $sort_field_placeholder = 'Sort by';
    private static $singular_name = 'Job Listing';
    private static $plural_name = 'Job Listings';
    private static $table_name = 'BBP_JobListings';
    private static $results_default_sort;
    private static $db = [
        'RecordID' => 'Varchar'
    ];
    private static $indexes = [
        'RecordID' => true
    ];
    private static $default_sort = 'Created Desc';

    /**
     * Keeps a temporary cache of field -> class mappings to prevent duplicated calls
     * @var array
     */
    private array $mappingCache = [];

    /**
     * Contains a copy of the data for the current record to avoid having to constantly query the storage
     * when doing things like the gridfield, etc.
     * @var array
     */
    private array $currentRecord = [];

    /**
     * Deal with storing the data in the elastic engine
     * @return void
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        if ($this->triggerBeforeWriteActions === true) {
            $dataConfig = $this->config()->get('fields');
            $data = [];

            foreach (array_keys($dataConfig) as $fieldName) {
                $data[$fieldName] = $this->$fieldName;
            }

            if ($this->RecordID == '') {
                //New record
                $data['recordcreated'] = time();
                $this->RecordID = ElasticHelper::createRecord($data);
            } else {
                //Update record
                ElasticHelper::updateRecord($this->RecordID, $data);
            }
        }
    }

    /**
     * Override the default write method to force an update even though the ORM doesn't think there are changes
     * to ensure things like onAfterWrite are fired
     * @param $showDebug
     * @param $forceInsert
     * @param $forceWrite
     * @param $writeComponents
     * @return int
     * @throws \SilverStripe\ORM\ValidationException
     */
    public function write($showDebug = false, $forceInsert = false, $forceWrite = false, $writeComponents = false)
    {
        return parent::write($showDebug, $forceInsert, true, $writeComponents);
    }


    /**
     * Disable the onBefore write actions for this instance
     * @return void
     */
    public function disableBeforeWriteActions()
    {
        $this->triggerBeforeWriteActions = false;
    }

    /**
     * Disable the onAfter write actions for this instance
     * @return void
     */
    public function disableAfterWriteActions()
    {
        $this->triggerAfterWriteActions = false;
    }

    /**
     * Disable all the before and after write actions for this instance
     * @return void
     */
    public function disableWriteActions()
    {
        $this->disableAfterWriteActions();
        $this->disableBeforeWriteActions();
    }

    /**
     * @param $member
     * @return bool
     */
    public function canView($member = null)
    {
        return true;
    }


    /**
     * Sorts out the summary fields from our schema
     * @return array|string[]
     */
    public function summaryFields()
    {
        $fields = [];

        foreach ($this->config()->get('fields') as $fieldName => $config) {
            if ((isset($config['cmssummaryfield'])) && ($config['cmssummaryfield'] === true)) {
                $fields[$fieldName] = $config['label'];
            }
        }

        if (count($fields) < 1) {
            $fields = [
                'ID' => 'ID',
                'RecordID' => 'Record ID'
            ];
        }
        return $fields;
    }

    /**
     * Remove the associated elastic record
     * @return void
     */
    public function onBeforeDelete()
    {
        parent::onBeforeDelete();
        ElasticHelper::deleteRecord($this->RecordID);
    }

    /**
     * Build the CMS fields for the record
     * @return \SilverStripe\Forms\FieldList
     * @throws \ReflectionException
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('RecordID')->setReadonly(true);
        $dataConfig = $this->config()->get('fields');
        $storedData = $this->getCurrentRecordData();

        foreach ($dataConfig as $fieldName => $config) {
            $config['fieldname'] = $fieldName;

            /**
             * @var JobField $fieldClass
             */
            $fieldClass = $this->getFieldClass($fieldName);
            $fieldClass->setFieldData($config);
            $fieldCMSField = $fieldClass->getCMSField();
            if ($fieldCMSField) {
                $fields->addFieldToTab('Root.Main', $fieldCMSField);
            }

            if ((isset($storedData[$fieldName])) &&
                ((!isset($config['hidden'])) || (($config['hidden'] !== true)))) {
                $cmsField = $fields->dataFieldByName($fieldName);
                if ($cmsField) {
                    $cmsField->setValue($fieldClass->formatForCMSField($storedData[$fieldName]));
                }
            }
        }

        return $fields;
    }

    /**
     * Get the document data for the current record
     * @return array
     */
    private function getCurrentRecordData(): array
    {
        $record = $this->getCurrentRecord();
        return (isset($record['_source'])) ? $record['_source'] : [];
    }

    /**
     * Get the current record from the storage
     * @return array
     */
    private function getCurrentRecord()
    {
        if ($this->isInDB()) {
            if (count($this->currentRecord) < 1) {
                if ($this->RecordID != '') {
                    $this->currentRecord = ElasticHelper::getRecordByID($this->RecordID);
                }
            }

            return $this->currentRecord;
        }
    }

    /**
     * Get the local field class for the given field
     * @param $fieldName
     * @return false|JobField|void
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getFieldClass($fieldName, $setConfig = true)
    {
        $fieldDefs = $this->config()->get('fields');
        $classMappings = $this->config()->get('field_mappings');

        if (!isset($fieldDefs[$fieldName])) {
            return false;
        }
        $fieldType = $fieldDefs[$fieldName]['type'];

        if (isset($classMappings[$fieldType])) {

            if (!isset($this->mappingCache[$fieldType])) {
                $fieldClassRef = new \ReflectionClass($classMappings[$fieldType]);
                $fieldClass = $fieldClassRef->newInstance();
                if ($setConfig === true) {
                    $fieldClass->setFieldData($fieldDefs[$fieldName]);
                }
                $this->mappingCache[$fieldType] = $fieldClass;
            }

            return $this->mappingCache[$fieldType];
        }

        throw new \Exception("No field class for " . $fieldName);
    }


    /**
     * Returns the last edited date of the record for the Cyberduck SEO module
     * @return string
     */
    public function getSitemapDate()
    {
        return $this->LastEdited;
    }


    /**
     * Magic getter to provide values for the fields as referenced by their field name
     * @param $property
     * @return mixed|DataObject|\SilverStripe\ORM\FieldType\DBField|null
     */
    public function __get($property)
    {
        if (strtolower($property) !== 'field' && $this->hasMethod($method = "get$property")) {
            return $this->$method();
        }
        if (($this->hasField($property)) && ($this->getField($property) != "")) {
            return $this->getField($property);
        }

        $currentRecord = $this->getCurrentRecordData();
        return (isset($currentRecord[$property])) ? $currentRecord[$property] : null;
    }

    /**
     * Implement a basic Link() method
     * @return string
     */
    public function Link()
    {
        $jobsPage = JobsPage::get_one(JobsPage::class);
        $pageURL = ($jobsPage) ? $jobsPage->Link() : '';
        return Controller::join_links([
            $pageURL,
            JobsPageController::getViewSegment(),
            $this->getURLSegment()
        ]);
    }


    /**
     * Absolute link to the job listing
     * @return mixed
     */
    function AbsoluteLink()
    {
        return Director::absoluteURL($this->Link());
    }


    /**
     * Attempt the creation of a SS-friendly title
     * @return string
     */
    public function getTitle(): string
    {
        $titleparts = [];
        $data = $this->getCurrentRecordData();

        //Find the fields which we want for the Title
        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['titlefield'])) && ($config['titlefield'] === true) && (isset($data[$fieldName]))) {
                $titleparts[] = $data[$fieldName];
            }
        }

        return implode(self::TITLE_SEPARATOR, $titleparts);
    }

    /**
     * @return void
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function onAfterWrite()
    {
        if ($this->triggerAfterWriteActions === true) {
            $data = [
                'urlsegment' => $this->getURLSegment(),
                'linkedid' => $this->ID
            ];

            if ($this->seotitle == '') {
                $data['seotitle'] = $this->getTitle();
            }

            ElasticHelper::updateRecord($this->RecordID, $data);
            JobsPageController::flush();
        }
        parent::onAfterWrite();
    }

    /**
     * Builds a URL segment based on the field definitions
     * @return string
     */
    public function getURLSegment()
    {
        $urlparts = [];

        //Find the fields which we want for the URL
        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['urlfield'])) && ($config['urlfield'] === true)) {
                $urlparts[] = $this->$fieldName;
            }
        }

        return Controller::join_links([
            $this->ID,
            URLSegmentFilter::create()->filter(implode("-", $urlparts))
        ]);
    }


    public static function getElasticIndexName()
    {
        if (self::$elasticIndexName == '') {
            self::setElasticIndexName();
        }
        return self::$elasticIndexName;
    }

    public static function setElasticIndexName()
    {
        $indexName = Environment::getEnv('ELASTIC_INDEX_NAME');
        if ($indexName == '') {
            throw new \Exception("No index name defined.");
        }
        self::$elasticIndexName = $indexName;
    }

}
