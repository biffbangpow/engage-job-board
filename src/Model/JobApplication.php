<?php

namespace BiffBangPow\JobBoard\Model;

use SilverStripe\ORM\DataObject;

class JobApplication extends DataObject
{
    private static $table_name = 'JobApplication';
    private static $form_class;
}
