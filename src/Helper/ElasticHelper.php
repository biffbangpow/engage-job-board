<?php

namespace BiffBangPow\JobBoard\Helper;

use BiffBangPow\JobBoard\Control\JobsPageController;
use BiffBangPow\JobBoard\Field\JobField;
use BiffBangPow\JobBoard\Model\JobListing;
use BiffBangPow\JobBoard\Service\ElasticService;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use ReflectionException;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

class ElasticHelper
{

    const DEPENDENT_FIELD_VALUE_SEPARATOR = '|';

    /**
     * Create a new record in the data store
     * @param $data
     * $data is an associative array of field_name (as specified in the yml config) and data which will
     * be manipulated by the field class assigned to the field.
     *
     * [
     *   'jobref' => '12345',
     *   'job-title' => 'Chief Test Engineer',
     *   'location' => 'Birmingham',
     *   'remote' => true,
     *   'consultant' => $variableContainingDataObject
     * ]
     */
    public static function createRecord($data)
    {
        $esData = self::transformData($data);

        return (new ElasticService())->indexDocument([
            'index' => JobListing::getElasticIndexName(),
            'id' => Uuid::uuid4()->toString(),
            'body' => $esData
        ]);

    }

    /**
     * Transform the data from the application, so it is ready
     * for storage in the data engine
     * @param $data
     * @return array
     */
    private static function transformData($data)
    {
        $esData = [];
        $jobListingClass = new JobListing();
        $dataConfig = JobListing::config()->get('fields');

        //Pass the data from the incoming array through the transform method in the class
        foreach ($data as $fieldName => $fieldData) {
            $fieldClass = $jobListingClass->getFieldClass($fieldName);

            //Populate the config for the field so the class can make any decisions needed
            $fieldConfig = $dataConfig[$fieldName];
            $fieldConfig['fieldName'] = $fieldName;
            $fieldClass->setFieldData($fieldConfig);

            $esData[$fieldName] = $fieldClass->formatForStorage($fieldData);
        }

        return $esData;
    }


    /**
     * Update an existing record
     * @param $recordID
     * @param array $data
     * @param bool $raw
     * @return bool
     */
    public static function updateRecord($recordID, array $data, bool $raw = false): bool
    {
        $esData = ($raw === true) ? $data : self::transformData($data);

        return (new ElasticService())->updateDocument([
            'index' => JobListing::getElasticIndexName(),
            'id' => $recordID,
            'body' => [
                "doc" => $esData
            ]
        ]);
    }

    /**
     * Delete a document from storage
     * @param $uuid
     * @return array
     */
    public static function deleteRecord($uuid)
    {
        return (new ElasticService())->deleteDocument([
            'index' => JobListing::getElasticIndexName(),
            'id' => $uuid
        ]);
    }

    /**
     * @throws ReflectionException
     */
    public static function jobsIndex($create = false)
    {
        $indexName = JobListing::getElasticIndexName();
        $properties = self::getFieldParams();

        echo "<h1>Building index " . $indexName . "</h1>";

        if ($create !== true) {
            $params = [
                'index' => $indexName,
                'body' => [
                    'properties' => $properties
                ]
            ];
        } else {
            $params = [
                'index' => $indexName,
                'body' => [
                    'settings' => self::getIndexSettings(),
                    'mappings' => [
                        'properties' => $properties
                    ]
                ]
            ];
        }

        echo "<pre>DEBUG:";
        print_r($params);
        echo "\n</pre>";

        $client = new ElasticService();
        return ($create === true) ? $client->createIndex($params) : $client->updateIndexFields($params);

    }

    /**
     * @return array|void
     * @throws ReflectionException
     */
    private static function getFieldParams()
    {
        $dataConfig = JobListing::config()->get('fields');
        $properties = [];
        $jobListingsClass = new JobListing();

        //Iterate the schema and work out what fields we need
        foreach ($dataConfig as $fieldName => $config) {
            /**
             * @var JobField $fieldClass
             */
            $fieldClass = $jobListingsClass->getFieldClass($fieldName);
            $fieldClass->setFieldData($config);
            $properties = array_merge($properties, $fieldClass->getFieldConfig($fieldName));
        }
        return $properties;
    }

    /**
     * Get the config for our index, including definitions for analysers, etc.
     * @return array
     */
    private static function getIndexSettings()
    {
        return [
            'number_of_shards' => 2,
            'analysis' => [
                'filter' => [
                    'filter_ps_en' => [
                        'type' => 'porter_stem',
                        'language' => 'English'
                    ]
                ],
                'analyzer' => [
                    'stemmed_analyzer' => [
                        'filter' => ['lowercase', 'filter_ps_en'],
                        'type' => 'custom',
                        'tokenizer' => 'whitespace',
                        'char_filter' => ['html_strip']
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $uuid
     * @return array
     */
    public static function getRecordByID($uuid)
    {
        return (new ElasticService())->getDocument([
            'index' => JobListing::getElasticIndexName(),
            'id' => $uuid
        ]);
    }

    /**
     * Get a job record from the elastic engine
     * @param $id
     * @return false|ArrayData
     */
    public static function getRecordByLinkedID($id, $format = true)
    {
        $esResult = (new ElasticService())->search([
            'index' => JobListing::getElasticIndexName(),
            'body' => [
                'query' => [
                    'match' => [
                        'linkedid' => $id
                    ]
                ]
            ]
        ]);

        $numResults = (int)$esResult['hits']['total']['value'];
        if ($numResults < 1) {
            return false;
        }

        if (isset($esResult['hits']['hits'])) {
            $sourceData = array_shift($esResult['hits']['hits']);
            $jobData = ($format === true) ? self::formatDataForTemplate($sourceData['_source']) : $sourceData['_source'];
            $jobData['_id'] = $sourceData['_id'];
            return ArrayData::create($jobData);
        }

        return false;
    }

    /**
     * Transform the data from the ES storage for output in a template
     * @param $data
     * @return array
     * @throws ReflectionException
     */
    private static function formatDataForTemplate($data)
    {
        $esData = [];
        $jobListingClass = new JobListing();

        //Pass the data from the incoming array through the transform method in the class
        foreach ($data as $fieldName => $fieldData) {
            $fieldClass = $jobListingClass->getFieldClass($fieldName);
            if ($fieldClass) {
                $esData[$fieldName] = $fieldClass->getDataForOutput($fieldData);
            }
        }

        return $esData;
    }

    /**
     * @param HTTPRequest $request
     * @param string $baseURL base URL of the job board, used for links
     * @param bool $forTemplate
     * @param int $from start record in the results
     * @param int $size page size
     * @return ArrayData|array
     * @throws ReflectionException
     */
    public static function getJobListings(
        HTTPRequest $request,
        string      $baseURL,
        bool        $forTemplate = false,
        int         $from = 0,
        int         $size = 24
    )
    {

        $params = [
            'index' => JobListing::getElasticIndexName()
        ];

        $queryParams = self::buildSearchTerms($request);
        if (count($queryParams) > 0) {
            $params['body'] = ['query' => $queryParams];
        }

        $params['body']['from'] = $from;
        $params['body']['size'] = $size;

        $sortOrder = self::getSortParams($request);

        if ($sortOrder) {
            $sortParams = self::buildSortFields($sortOrder);
            if ($sortParams) {
                $params['body']['sort'] = $sortParams;
            }
        }

        //Injector::inst()->get(LoggerInterface::class)->info(print_r($params, true));

        $records = (new ElasticService())->search($params);
        return self::processSearchData($records, $baseURL, $forTemplate, $from, $size);

    }


    /**
     * Get job data based on a set of filters
     * @param array $params
     * @param string $baseURL
     * @param bool $forTemplate
     * @param int $from
     * @param int $size
     * @return array|ArrayData
     */
    public static function getJobsByFilterParams(array $params, string $baseURL, bool $forTemplate = false, int $from = 0, int $size = 24, $sortOrder = false)
    {
        $searchParams = [];
        $queryParams = [];

        //See which fields are available to filter
        $filterFields = self::getFilterFields();

        //Ensure we're only attempting to filter on fields we're allowed to
        $fieldsToFilter = array_intersect(array_keys($filterFields), array_keys($params));

        foreach ($fieldsToFilter as $fieldName) {
            $searchParams[] = self::getTermsFilter($fieldName, $params[$fieldName]);
        }

        if (count($searchParams) > 0) {
            $queryParams = ['bool' => [
                'must' => $searchParams
            ]];
        }

        $elasticParams = [
            'index' => JobListing::getElasticIndexName()
        ];

        if (count($queryParams) > 0) {
            $elasticParams['body'] = ['query' => $queryParams];
        }

        $elasticParams['body']['from'] = $from;
        $elasticParams['body']['size'] = $size;

        if ($sortOrder) {
            $sortParams = self::buildSortFields($sortOrder);
            if ($sortParams) {
                $elasticParams['body']['sort'] = $sortParams;
            }
        }


        $records = (new ElasticService())->search($elasticParams);

        return self::processSearchData($records, $baseURL, $forTemplate, $from, $size);
    }


    /**
     * Process the data from an elastic search for use by the application
     * @param $records
     * @param bool $forTemplate
     * @param int $from
     * @param int $size
     * @return array|ArrayData
     * @throws ReflectionException
     */
    private static function processSearchData($records, $baseURL, bool $forTemplate = false, int $from = 0, int $size = 24)
    {
        $result = ($forTemplate === true) ? ArrayList::create() : [];

        if ($forTemplate === true) {
            $jobListingClass = new JobListing();
        }

        foreach ($records['hits']['hits'] as $record) {

            if ($forTemplate === true) {
                //We need to pass the data through the output formatter in the relevant field class

                $recordData = [];

                foreach ($record['_source'] as $fieldName => $fieldData) {
                    $fieldClass = $jobListingClass->getFieldClass($fieldName, true);
                    if ($fieldClass) {
                        $recordData[$fieldName] = $fieldClass->getDataForOutput($fieldData);
                    }
                }

                $result->push(ArrayData::create([
                    'ID' => $record['_id'],
                    'Data' => $recordData
                ]));
            } else {
                $result[] = [
                    'ID' => $record['_id'],
                    'Data' => $record['_source']
                ];
            }
        }

        $jobData = [
            'QueryTime' => $records['took'],
            'TotalHits' => $records['hits']['total']['value'],
            'BaseURL' => $baseURL,
            'ViewSegment' => JobsPageController::getViewSegment(),
            'Jobs' => $result
        ];

        if ($forTemplate === true) {
            return ArrayData::create($jobData);
        }

        return $jobData;
    }


    /**
     * Build the parameters for passing into the elastic query
     * @param HTTPRequest $request
     * @return array|array[]
     */
    private static function buildSearchTerms(HTTPRequest $request)
    {
        //Get all the fields which are searchable or filterable
        $searchFields = self::getSearchFields();
        $filterFields = self::getFilterFields();

        $validFields = array_merge($searchFields, $filterFields);
        //$fieldsToFilter = array_intersect(array_keys($validFields), array_keys($request->postVars()));
        $fieldsToFilter = array_keys($filterFields);

        $searchParams = [];
        $queryParams = [];

        //Start with search
        $frontEndSearchFieldName = JobListing::config()->get('search_field_name');
        $searchTerm = $request->postVar($frontEndSearchFieldName);

        if ((count($searchFields) > 0) && ($searchTerm != '')) {
            $searchParams[] = [
                'multi_match' => [
                    'query' => $searchTerm,
                    'fields' => array_keys($searchFields)
                ]
            ];
        }

        if (count($fieldsToFilter) > 0) {
            foreach ($fieldsToFilter as $fieldName) {
                $filterParams = self::getFilterParams($fieldName, $request);
                if (is_array($filterParams)) {
                    $searchParams = array_merge($searchParams, $filterParams);
                }
            }
        }

        if (count($searchParams) > 0) {
            $queryParams = ['bool' => [
                'must' => $searchParams
            ]];
        }

        return $queryParams;
    }


    private static function getFilterParams($fieldName, HTTPRequest $request)
    {

        $fieldClass = false;

        $fieldDefs = JobListing::config()->get('fields');
        if ((isset($fieldDefs[$fieldName])) && (isset($fieldDefs[$fieldName]['frontend-field']))) {
            $frontendFieldType = $fieldDefs[$fieldName]['frontend-field'];
            $frontendClassMapping = JobListing::config()->get('frontend_field_mappings');
            if (isset($frontendClassMapping[$frontendFieldType])) {
                $fieldClass = $frontendClassMapping[$frontendFieldType];
            }
        }

        if ($fieldClass !== false) {

            $ref = new \ReflectionClass($fieldClass);
            $field = $ref->newInstanceArgs([
                $fieldName,
                $fieldDefs[$fieldName]
            ]);
            return $field->getTermsForFilter($request);
        }

        return false;
    }


    /**
     * Get all the fields which are set as searchable in the config
     * @return array
     */
    public static function getSearchFields(): array
    {
        $searchFields = [];
        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['searchable'])) && ($config['searchable'] === true)) {
                $searchFields[$fieldName] = $config;
            }
        }
        return $searchFields;
    }

    /**
     * Get all the fields which are set as filterable in the config
     * @return array
     */
    private static function getFilterFields(): array
    {
        $filterFields = [];
        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['filterable'])) && ($config['filterable'] === true)) {
                $filterFields[$fieldName] = $config;
            }
        }
        return $filterFields;
    }

    /**
     * Get all the fields which are set as sort fields in the config
     * @return array
     */
    public static function getSortFields(): array
    {
        $sortFields = [];
        foreach (JobListing::config()->get('fields') as $fieldName => $config) {
            if ((isset($config['sortfield'])) && ($config['sortfield'] === true)) {
                $sortFields[$fieldName] = $config;
            }
        }
        return $sortFields;
    }


    /**
     * Get the structure for a specific field filter
     * @param $fieldName
     * @param $matchValue
     * @return array[]|\array[][]
     */
    private static function getTermsFilter($fieldName, $matchValue)
    {
        if (is_array($matchValue)) {
            return ['terms' => [$fieldName => $matchValue]];
        } else {
            return ['term' => [
                $fieldName => ['value' => $matchValue]
            ]];
        }
    }

    /**
     * Gets all the possible combinations of the configured fields and returns a nested array
     * @param string $fieldName
     * @param $config
     * @param int $maxValues
     * @return array
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @todo Currently this only allows for a single level of nesting.  Could be extended to go further if needed,  but we'll have to rework the PHP array tranformation
     */
    public static function getDependentFieldValues(string $fieldName, $config, int $maxValues = 100)
    {
        $sourceFields = $config['fields'];

        if ((!is_array($sourceFields)) || (count($sourceFields) < 1)) {
            throw new \Exception('Dependent fields config requires an array of source fields');
        }

        if (count($sourceFields) !== 2) {
            throw new \Error('Dependent fields only work with 2 levels of nesting');
        }

        $scriptSource = [];
        $out = [];

        $out['fieldnames'] = $sourceFields;

        foreach ($sourceFields as $sourceField) {
            $scriptSource[] = sprintf("doc['%s'].value", $sourceField);
            $out['fieldlabels'][] = FieldHelper::getFieldLabelFromConfig($sourceField);
        }

        $script = implode(" + '" . self::DEPENDENT_FIELD_VALUE_SEPARATOR . "' + ", $scriptSource) . ';';

        $params = [
            'index' => JobListing::getElasticIndexName(),
            'body' => [
                'size' => 0,
                'aggs' => [
                    'values' => [
                        'terms' => [
                            'size' => $maxValues,
                            'script' => [
                                'source' => $script
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $resData = (new ElasticService())->search($params);
        $results = [];

        if (!isset($resData['aggregations']['values'])) {
            Injector::inst()->get(LoggerInterface::class)->info("Result contains no aggregation data");
            Injector::inst()->get(LoggerInterface::class)->info(print_r($resData, true));
            return $out;
        }

        foreach ($resData['aggregations']['values']['buckets'] as $aggData) {
            $results[] = $aggData['key'];
        }


        foreach ($results as $result) {
            $theseValues = explode(self::DEPENDENT_FIELD_VALUE_SEPARATOR, $result);

            $field1Value = $theseValues[0];
            $field2Value = $theseValues[1];

            if (!isset($out['values'][$field1Value])) {
                $out['values'][$field1Value] = [];
            }

            $out['values'][$field1Value][] = $field2Value;
        }

        return $out;
    }


    /**
     * Get all the possible values for a field.  If $assoc is set true, the array will be
     * associative, with the key and value matching, else a simple numeric array is returned
     * an option array of filters can be passed to limit the records we're aggregating
     * @param string $fieldName
     * @param bool $assoc
     * @param bool|array $filters
     * @param int $maxValues
     * @return array
     */
    public static function getDistinctValuesForField(string $fieldName, bool $assoc = false, bool|array $filters = false, int $maxValues = 100): array
    {
        $result = [];

        $params = [
            'index' => JobListing::getElasticIndexName(),
            'body' => [
                'size' => 0,
                'aggs' => [
                    $fieldName => [
                        'terms' => [
                            'field' => $fieldName,
                            'size' => $maxValues,
                            'order' => [
                                "_key" => "asc"
                            ]
                        ]
                    ]
                ]
            ]
        ];

        if ($filters !== false) {

            $fieldsToFilter = array_keys($filters);
            $searchParams = [];
            foreach ($fieldsToFilter as $filterFieldName) {
                $searchParams[] = self::getTermsFilter($filterFieldName, $filters[$filterFieldName]);
            }

            if (count($searchParams) > 0) {
                $queryParams = ['bool' => [
                    'must' => $searchParams
                ]];

                $params['body']['query'] = $queryParams;
            }
        }

        $resData = (new ElasticService())->search($params);

        if (!isset($resData['aggregations'][$fieldName])) {
            return $result;
        }

        foreach ($resData['aggregations'][$fieldName]['buckets'] as $aggData) {
            $term = $aggData['key'];
            if ($assoc === true) {
                $result[$term] = $term;
            } else {
                $result[] = $term;
            }
        }

        return $result;
    }

    /**
     * Get the sort parameter from the request and return it
     * @param HTTPRequest $request
     * @return bool|string
     */
    private static function getSortParams(HTTPRequest $request)
    {
        $sortParamName = JobListing::config()->get('sort_field_name');

        if ($request->requestVar($sortParamName) != '') {
            return $request->requestVar($sortParamName);
        }
        return self::getDefaultSortOrder();
    }


    /**
     * Get the default sort order if it's defined in the config
     * @return string|false
     */
    public static function getDefaultSortOrder(): bool|string
    {
        $sort = JobListing::config()->get('results_default_sort');
        //Injector::inst()->get(LoggerInterface::class)->info('Default sort: ' . $sort);
        return ($sort != "") ? $sort : false;
    }


    /**
     * Build the sorting parameters for the elastic query
     * @param string $sortString in the format  fieldname:sortdirection  (eg.  date:desc)
     * @return array|false
     */
    private static function buildSortFields(string $sortString): bool|array
    {
        if (trim($sortString) != "") {
            $parts = explode(":", $sortString);

            if ($parts && (count($parts) > 0)) {
                $sortField = $parts[0];
                $sortFields = self::getSortFields();

                $sortOrder = 'asc';
                if (isset($sortFields[$sortField])) {
                    if (isset($parts[1])) {
                        $sortOrder = (trim($parts[1]) === 'desc') ? 'desc' : 'asc';
                    }
                    return [
                        $sortField => [
                            'order' => $sortOrder
                        ]
                    ];
                }
            }
        }
        return false;
    }


    /**
     * Delete records which do not contain specific values for a field
     * @param $excludeField
     * @param $excludeValue
     * @return void
     */
    public static function deleteRecordsByExclusion($excludeField, $excludeValue)
    {

        $params = [
            'index' => JobListing::getElasticIndexName(),
            'body' => [
                'query' => [
                    'bool' => [
                        'must_not' => [
                            'terms' => [$excludeField => (array)$excludeValue]
                        ]
                    ]
                ]
            ]
        ];

        Injector::inst()->get(LoggerInterface::class)->info(print_r($params));

        $res = (new ElasticService())->deleteByQuery($params);

    }


}
