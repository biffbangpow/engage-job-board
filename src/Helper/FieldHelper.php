<?php

namespace BiffBangPow\JobBoard\Helper;

use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Core\Config\Config;

class FieldHelper
{

    /**
     * Get the front-end config values for the given field
     * @param $fieldName
     * @param $config
     * @return string
     * @throws \ReflectionException
     */
    public static function getFrontEndDefs($fieldName, $config)
    {
        $frontendFieldType = $config['frontend-field'];
        $fieldMapping = Config::inst()->get(JobListing::class, 'frontend_field_mappings');
        if (isset($fieldMapping[$frontendFieldType])) {
            $ref = new \ReflectionClass($fieldMapping[$frontendFieldType]);
            $fieldClass = $ref->newInstanceArgs([
                $fieldName,
                $config
            ]);

            return $fieldClass->getConfigValues();
        }
        return '';
    }

    /**
     * Gets the front-end label for a given field
     * @param $fieldName
     * @return mixed|null
     */
    public static function getFieldLabelFromConfig($fieldName) {
        $fieldsConfig = JobListing::config()->get('fields');
        if (isset($fieldsConfig[$fieldName]) && (isset($fieldsConfig[$fieldName]['label']))) {
            return $fieldsConfig[$fieldName]['label'];
        }
        return null;
    }
}
