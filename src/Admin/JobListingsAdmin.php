<?php

namespace BiffBangPow\JobBoard\Admin;

use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Dev\CsvBulkLoader;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\Forms\GridField\GridFieldExportButton;
use SilverStripe\Forms\GridField\GridFieldImportButton;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;

class JobListingsAdmin extends ModelAdmin
{
    private static $managed_models = [
        JobListing::class
    ];
    private static $menu_title = 'Job Listings';
    private static $url_segment = 'jobs-admin';
    private static $model_importers = [];
    private static $menu_icon_class = 'font-icon-accessibility';

    protected function getGridFieldConfig(): GridFieldConfig
    {
        $config = parent::getGridFieldConfig();
        $config->removeComponentsByType(GridFieldImportButton::class);
        $config->removeComponentsByType(GridFieldExportButton::class);
        return $config;
    }
}
