import {createApp} from 'vue'

const axios = require('axios');
const queryString = require('query-string');

import FilterInput from "@/components/FilterInput";
import SortDropdown from "@/components/SortDropdown";

const moment = require('moment');

const mountEl = document.querySelector("#app");

createApp({
    data() {
        return {
            queryString,
            fieldsConfig: {},
            sortConfig: {},
            sort: '',
            sortFieldname: '',
            sortPlaceholder: '',
            results: '',
            totalResults: 0,
            baseURL: '',
            from: 0,
            size: this.defaultPageSize,
            filterValues: {},
            loading: true,
        }
    },
    props: [
        'resultsLink',
        'fieldsConfigLink',
        'defaultPageSize'
    ],
    components: {
        FilterInput,
        SortDropdown
    },
    computed: {
        totalPages() {
            return Math.ceil(this.totalResults / this.size);
        },
        currentPage() {
            return (this.from / this.size) + 1;
        },
        shouldShowPaginationNext() {
            return (this.from + this.size < this.totalResults)
        },
        shouldShowPaginationPrev() {
            return (this.from >= this.size)
        },
        paginateNextLink() {
            let nextPage = this.from + this.size;
            return this.baseURL + '?from=' + nextPage + '&size=' + this.size;
        },
        paginatePrevLink() {
            let prevPage = this.from - this.size;
            return this.baseURL + '?from=' + prevPage + '&size=' + this.size;
        },
    },
    methods: {
        paginateNext() {
            this.from += this.size;
            this.getResults(true);
        },
        paginatePrev() {
            this.from -= this.size;
            this.getResults(true);
        },
        paginateToPage(pageNum) {
            this.from = (pageNum - 1) * this.size;
            this.getResults(true);
        },
        paginateToPageLink(pageNum) {
            return this.baseURL + '?from=' + ((pageNum - 1) * this.size) + '&size=' + this.size;
        },
        paginationClass(pageNum) {
            if ((pageNum - 1) * this.size === this.from) {
                return 'current';
            }
            return '';
        },
        getJobLink(job) {
            return this.baseURL + 'view/' + job.Data.urlsegment;
        },
        clearAllFilterValues() {
            this.filterValues = {};
            let filterInputs = this.$refs.jbfilterinput;
            if (filterInputs !== undefined) {
                for (var i = 0; i < filterInputs.length; i++) {
                    filterInputs[i].clearData();
                }
            }
            let pageURL = new URL(window.location.href);
            let pageBaseURL = pageURL.origin + pageURL.pathname;
            const pageTitle = document.getElementsByTagName("title")[0].innerHTML;
            window.history.replaceState({}, pageTitle, pageBaseURL);
            this.setFilterValuesFromFieldsConfig();
        },
        setFilterValuesFromFieldsConfig() {
            let pageURL = new URL(window.location.href);

            for (let item in this.fieldsConfig) {
                if ('multiple' in this.fieldsConfig[item] && (this.fieldsConfig[item]['multiple'] === true)) {
                    if (pageURL.searchParams.getAll(item + '[]') !== null) {
                        this.filterValues[item] = pageURL.searchParams.getAll(item + '[]');
                    } else {
                        this.filterValues[item] = [];
                    }
                } else if (this.fieldsConfig[item]['type'] === 'slider') {
                    if (
                        pageURL.searchParams.get(item + '_unit') !== null &&
                        pageURL.searchParams.get(item + '_min') !== null &&
                        pageURL.searchParams.get(item + '_max') !== null
                    ) {
                        this.filterValues[item] = {
                            unit: pageURL.searchParams.get(item + '_unit'),
                            min: pageURL.searchParams.get(item + '_min'),
                            max: pageURL.searchParams.get(item + '_max')
                        };
                    } else {
                        this.filterValues[item] = {
                            unit: undefined,
                            min: undefined,
                            max: undefined
                        };
                    }
                } else if (this.fieldsConfig[item]['type'] === 'dependent') {

                    let dependantConfig = {};

                    this.fieldsConfig[item].values.fieldnames.forEach(function (item) {
                        if (
                            pageURL.searchParams.get(item) !== null
                        ) {
                            dependantConfig[item] = pageURL.searchParams.get(item);
                        } else {
                            dependantConfig[item] = '';
                        }
                    });

                    this.filterValues[item] = dependantConfig;

                } else {
                    if (pageURL.searchParams.get(item) !== null) {
                        this.filterValues[item] = pageURL.searchParams.get(item);
                    } else {
                        this.filterValues[item] = '';
                    }
                }
            }

            if (pageURL.searchParams.get('size') !== null) {
                this.size = parseInt(pageURL.searchParams.get('size'));
            } else {
                this.size = this.defaultPageSize;
            }

            if (pageURL.searchParams.get('from') !== null) {
                this.from = parseInt(pageURL.searchParams.get('from'));
            } else {
                this.from = 0;
            }

            if (pageURL.searchParams.get(this.sortConfig.fieldname) !== null) {
                this.sort = pageURL.searchParams.get(this.sortConfig.fieldname);
            } else {
                this.sort = this.sortConfig.defaultsort;
            }

            this.getResults(false);
        },
        async getFieldsConfig() {
            axios.get(this.fieldsConfigLink).then(response => {
                this.fieldsConfig = response.data.fields;
                this.sortConfig = response.data.sort;
                this.sort = this.sortConfig.defaultsort;
                this.setFilterValuesFromFieldsConfig();
            });
        },
        addFilterValuesToURL() {
            let pageURL = new URL(window.location.href);
            let pageBaseURL = pageURL.origin + pageURL.pathname;

            const nextURL = new URL(pageBaseURL);

            for (let filter in this.filterValues) {
                if (this.filterValues[filter] !== '') {
                    if (filter in this.fieldsConfig && 'multiple' in this.fieldsConfig[filter] && (this.fieldsConfig[filter]['multiple'] === true)) {
                        for (var i = 0; i < this.filterValues[filter].length; i++) {
                            nextURL.searchParams.append(filter + '[]', this.filterValues[filter][i]);
                        }
                    } else if (filter in this.fieldsConfig && (this.fieldsConfig[filter]['type'] === 'slider')) {
                        if (this.filterValues[filter] !== {}) {
                            if (
                                this.filterValues[filter]['unit'] !== undefined &&
                                this.filterValues[filter]['min'] !== undefined &&
                                this.filterValues[filter]['max'] !== undefined
                            ) {
                                nextURL.searchParams.append(filter + '_unit', this.filterValues[filter]['unit']);
                                nextURL.searchParams.append(filter + '_min', this.filterValues[filter]['min']);
                                nextURL.searchParams.append(filter + '_max', this.filterValues[filter]['max']);
                            }
                        }
                    } else if (filter in this.fieldsConfig && (this.fieldsConfig[filter]['type'] === 'dependent')) {
                        for (const [key, value] of Object.entries(this.filterValues[filter])) {
                            if (value !== undefined && value !== '') {
                                nextURL.searchParams.append(key, value);
                            }
                        }
                    } else {
                        nextURL.searchParams.append(filter, this.filterValues[filter]);
                    }
                }
            }

            const pageTitle = document.getElementsByTagName("title")[0].innerHTML;
            window.history.replaceState({}, pageTitle, nextURL.href);
        },
        getResults(paginating) {

            this.loading = true;
            console.log('Loading started');

            // This SHOULD always be set but setting a default just in case it isn't
            if (paginating === undefined) {
                paginating = false;
            }

            if (paginating === false) {
                this.from = 0;
            }

            let bodyFormData = new FormData();

            this.filterValues['from'] = this.from;
            this.filterValues['size'] = this.size;
            this.filterValues[this.sortConfig.fieldname] = this.sort;

            for (let filter in this.filterValues) {
                if (filter in this.fieldsConfig && 'multiple' in this.fieldsConfig[filter] && (this.fieldsConfig[filter]['multiple'] === true)) {
                    for (var i = 0; i < this.filterValues[filter].length; i++) {
                        bodyFormData.append(filter + '[]', this.filterValues[filter][i]);
                    }
                } else if (filter in this.fieldsConfig && (this.fieldsConfig[filter]['type'] === 'slider')) {
                    if (
                        this.filterValues[filter]['unit'] !== undefined &&
                        this.filterValues[filter]['min'] !== undefined &&
                        this.filterValues[filter]['max'] !== undefined
                    ) {
                        bodyFormData.append(filter + '_unit', this.filterValues[filter]['unit']);
                        bodyFormData.append(filter + '_min', this.filterValues[filter]['min']);
                        bodyFormData.append(filter + '_max', this.filterValues[filter]['max']);
                    }
                } else if (filter in this.fieldsConfig && (this.fieldsConfig[filter]['type'] === 'dependent')) {
                    for (const [key, value] of Object.entries(this.filterValues[filter])) {
                        if (value !== undefined && value !== '') {
                            bodyFormData.append(key, value);
                        }
                    }
                } else {
                    bodyFormData.append(filter, this.filterValues[filter]);
                }
            }

            let url = this.resultsLink;

            axios.post(
                url,
                bodyFormData,
                {
                    headers: {
                        'content-type': 'application/x-www-form-urlencoded'
                    }
                }
            ).then(response => {
                this.results = response.data.Jobs;
                this.totalResults = response.data.TotalHits;
                this.baseURL = response.data.BaseURL;
                this.addFilterValuesToURL();
                this.loading = false;
                console.log('Loading finished');
            });
        },
        formatDate(dateInput, dateFormat) {
            if (dateInput === undefined) {
                return '';
            }
            if (dateFormat === undefined) {
                dateFormat = 'Do MMMM YYYY';
            }
            return moment(dateInput).format(dateFormat);
        }
    },
    async created() {
        await this.getFieldsConfig();
    },
}, mountEl.dataset).mount('#app');
