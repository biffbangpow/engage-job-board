const applicationForm = document.getElementById('ApplicationForm_ApplicationForm');
const applicationMessage = document.getElementById('applicationform-message');
const applicationSubmit = document.getElementById('ApplicationForm_ApplicationForm_action_processApplication');

const handleApplication = function () {
    let pristine = new Pristine(applicationForm);
    if (pristine.validate()) {
        applicationSubmit.disabled = true;
        if (applicationMessage !== null) {
            applicationMessage.innerHTML = '<p>Please wait a moment..</p>';
        }
        let FD = new FormData(applicationForm);
        let req = new XMLHttpRequest();
        req.addEventListener('load', applicationResponse);
        req.open("POST", applicationForm.action);
        req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        req.send(FD);
    } else {
        alert("Please fill in all fields on the application form, accept the privacy terms and don't forget to attach your CV");
    }
}

window.handleApplication = handleApplication;

const applicationResponse = function (res) {
    let responseData = JSON.parse(res.target.responseText);
    applicationSubmit.disabled = false;
    if (applicationMessage !== null) {
        applicationMessage.innerHTML = '<p>' + responseData.message + '</p>';
    }
    if (responseData.status === 1) {
        //Success - clear the form
        applicationForm.reset();
        if (typeof jobBoardApplicationSuccess === 'function') {
            jobBoardApplicationSuccess();
        }
    }
}

const initUploadFields = function () {
    document.querySelectorAll('.bbp-file-upload').forEach((uploadElem) => {

        let inputField = uploadElem.querySelector(':scope input.file-input');
        let button = uploadElem.querySelector(':scope div.bbp-file-upload-btn');
        let fieldLabel = uploadElem.querySelector(':scope label.file-label');

        inputField.addEventListener("change", () => {
            let fileName = inputField.value.replace(/^.*\\/, "");
            if (fileName !== '' && fileName !== undefined) {
                let fileSize = inputField.files[0].size / 1024 / 1024;
                fileSize = Math.round(fileSize * 100) / 100;
                fieldLabel.textContent = fileName;
                button.textContent = fileSize + 'MB';
            }
        });

    });
}

document.addEventListener("DOMContentLoaded", () => {
    MicroModal.init();
    initUploadFields();

    if (!applicationSubmit.classList.contains('g-recaptcha')) {
        applicationForm.addEventListener('submit', (e) => {
            e.preventDefault();
            handleApplication();
        });
    }
});
