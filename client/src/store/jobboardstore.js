import {defineStore} from 'pinia'

export const useJobBoardStore = defineStore('storeId', {
    // arrow function recommended for full type inference
    state: () => {
        return {
            filterValues: {}
        }
    },
    actions: {
        addFilterValue(key, value) {
            this.filterValues[key] = value;
        },
        bulkAddFilterValues(filterValues) {
            for (let item in filterValues) {
                this.addFilterValue(item, filterValues[item]);
            }
        },
        clearFilterValue(key) {
            delete this.filterValues[key];
        },
        clearAllFilterValues() {
            this.filterValues = {};
        }
    },
    getters: {
        getFilterValue: (state) => {
            return (filterValueKey) => state.filterValues[filterValueKey];
        },
    },
    persist: true
})
