const {defineConfig} = require('@vue/cli-service')
const CopyPlugin = require("copy-webpack-plugin");

module.exports = defineConfig({
    transpileDependencies: true,
    filenameHashing: false,
    runtimeCompiler: true,
    pages: {
        app: {
            entry: './src/main.js',
        },
    },
    configureWebpack: {
        plugins: [
            new CopyPlugin({
                patterns: [
                    { from: "src/application", to: "application" },
                ],
            }),
        ],
    }
})
