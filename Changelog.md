# Engage Job Board

## Changelog


### v2.3.1 - Sept 3rd, 2024

This verson adds support for the Cyberbuck SEO module.  On-page meta and sitemap data are automatically added to the module's functionality from the job listings.


### v2.3.0 - Sept 2nd, 2024

This release improves the SEO functionality of the board.  It provides for better integration with the Wilr sitemaps module, and ensures that job data is pushed into the page at render time so the meta content is correctly populated.

To add jobs to the Wilr sitemap, add the following code to `_config.php`:

```php
GoogleSitemap::register_dataobject(JobListing::class, "weekly");
```

_You will also need to ensure you have the correct 'use' statements in place at the top of the file_

The SEO features are designed to work with the `innoweb/silverstripe-social-metadata` package.   If this package is not in use, custom code may be required to recreate the functionality.

The job board now populates the following fields in the SEO module:

- Page title (automatically reflected in the OG title field)
- Meta description (automatically reflected in the OG description field)
- Canonical link (automatically reflected in the OG url field)

An extension point has been provided in the `view` method of the `JobsPageController` class to allow the automatically generated data to be overridden.