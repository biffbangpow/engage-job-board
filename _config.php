<?php

use BiffBangPow\JobBoard\Model\JobListing;
use SilverStripe\Core\Manifest\ModuleLoader;
use Wilr\GoogleSitemaps\GoogleSitemap;

$module = ModuleLoader::getModule('wilr/silverstripe-googlesitemaps');
if ($module) {
    GoogleSitemap::register_dataobject(JobListing::class);
}